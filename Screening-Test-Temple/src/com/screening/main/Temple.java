package com.screening.main;

import com.screening.serviceImpl.StepImpl;
import java.util.Scanner;

public class Temple {
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        System.out.print("Enter no. of Stairs of Temple = ");
        int stairs=sc.nextInt();

        StepImpl Johan =new StepImpl();
        int ways=Johan.climb(stairs);
        System.out.print("Number of Possible Ways= "+ways);

    }
}
