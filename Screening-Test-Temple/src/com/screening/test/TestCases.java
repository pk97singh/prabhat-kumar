package com.screening.test;

import com.screening.serviceImpl.StepImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TestCases {

    @Test
    public void testClimb(){
        int expectedResult = 274;
        StepImpl Johan_Test =new StepImpl();
        int ways = Johan_Test.climb(10);
        assertEquals(274,ways);
        System.out.println("Test Case Passed");

    }

}
