package com.screening.serviceImpl;

import com.screening.service.Steps;

public class StepImpl implements Steps {
    public int climb(int steps) {
        if (steps == 1 || steps == 0)
            return 1;
        else if (steps == 2)
            return 2;
        else
            return climb(steps - 3) + climb(steps - 2) + climb(steps - 1);
    }
}
